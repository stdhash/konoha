'use strict';

import {describe, it} from 'mocha';
import Konoha from '../index';
import 'should';

describe('Konoha', function() {
	it('should return same objects from set', function() {
		var app = new Konoha();
		app.set('testObj', function() {
			return {};
		});

		app.get('testObj').should.be.exactly(app.get('testObj'));
	});

	it('should return exact value if not a function - string', function() {
		var app = new Konoha();
		var str = 'Test String';
		app.set('testStr', str);
		app.get('testStr').should.be.exactly(str);
	});

	it('should return exact value if not a function - integer', function() {
		var app = new Konoha();
		var num = 123;
		app.set('testNum', num);
		app.get('testNum').should.be.exactly(num);
	});

	it('should be same app instance', function() {
		var app = new Konoha();
		var app2;
		app.set('testObj', function() {
			app2 = this;
			return {};
		});

		app.get('testObj').should.be.exactly(app.get('testObj'));
		app2.should.be.exactly(app);
	});

	it('should return different objects from factory', function() {
		var app = new Konoha();
		app.factory('testFactory', function() {
			return {};
		});

		app.get('testFactory').should.not.be.exactly(app.get('testFactory'));
	});

	it('should return protected object', function() {
		var app = new Konoha();
		var obj = {};
		app.protect('protectObj', obj);
		app.get('protectObj').should.be.exactly(obj);
	});

	it('should return protected function', function() {
		var app = new Konoha();
		var func = function() {};
		app.protect('protectFunc', func);
		app.get('protectFunc').should.be.exactly(func);
	});

	it('should extend object', function() {
		var app = new Konoha();
		var obj = {};
		var value = 123;
		app.set('extendObj', obj);
		app.extend('extendObj', function(original) {
			original.value = value;
			return original;
		});
		var extendObj = app.get('extendObj');
		extendObj.should.be.exactly(obj);
		app.get('extendObj').value.should.be.exactly(value);
	});

	it('should get all keys', function() {
		var app = new Konoha();
		app.set('keySetObj', {});
		app.set('keySetFunc', function() {});
		app.factory('keyFactoryObj', {});
		app.factory('keyFactoryFunc', function() {});
		app.protect('keyProtectObj', {});
		app.protect('keyProtectFunc', function() {});

		var keys = app.keys();
		keys.should.containEql('keySetObj');
		keys.should.containEql('keySetFunc');
		keys.should.containEql('keyFactoryObj');
		keys.should.containEql('keyFactoryFunc');
		keys.should.containEql('keyProtectObj');
		keys.should.containEql('keyProtectFunc');
	});

	it('should check if it has service', function() {
		var app = new Konoha();
		app.set('keySetObj', {});
		app.factory('keyFactoryFunc', function() {});
		app.protect('keyProtectObj', {});
		app.protect('keyProtectFunc', function() {});

		app.has('keySetObj').should.be.exactly(true);
		app.has('keySetFunc').should.be.exactly(false);
		app.has('keyFactoryObj').should.be.exactly(false);
		app.has('keyFactoryFunc').should.be.exactly(true);
		app.has('keyProtectObj').should.be.exactly(true);
		app.has('keyProtectFunc').should.be.exactly(true);
	});

	it('should be able to register service', function() {
		var app = new Konoha();
		var service = function() {};
		var app2 = app.register(service);
		app2.should.be.exactly(app);
	});
});
