class Konoha {
	constructor() {
		this._values = {};
		this._factories = {};
		this._protected = {};
		this._frozen = {};
		this._raw = {};
	}

	get(id) {
		if (this.raw.hasOwnProperty(id) ||
			this._protected.hasOwnProperty(id) ||
			typeof this._values[id] !== 'function'
		) {
			return this._values[id];
		}

		if (this._factories.hasOwnProperty(id)) {
			return this._values[id].call(this);
		}

		this._raw[id] = this._values[id];
		this._values[id] = this._raw[id].call(this);
		this._frozen[id] = true;

		return this._values[id];
	}

	set(id, service) {
		if (this._frozen.hasOwnProperty(id)) {
			return new Error('Cannot override frozen service');
		}

		this._values[id] = service;
		return this;
	}

	factory(id, factory) {
		if (typeof factory === 'function') {
			this._factories[id] = true;
		}

		this._values[id] = factory;
		return this;
	}

	protect(id, service) {
		this._protected[id] = true;
		this._values[id] = service;
		return this;
	}

	extend(id, extender) {
		if (!this._values.hasOwnProperty(id)) {
			return new Error('Identifier ' + id + 'is not defined');
		}

		if (typeof extender !== 'function') {
			return new Error('Extension service definition is not a Class or Function');
		}

		var factory = this._values[id];
		var method = 'set';
		var call = true;

		if (this._factories.hasOwnProperty(id)) {
			method = 'factory';
		} else if (this._protected.hasOwnProperty(id)) {
			method = 'protect';
			call = false;
		}

		if (typeof factory !== 'function') {
			call = false;
		}

		return this[method](id, function() {
			return extender.call(this, call ? factory.call(this) : factory);
		});
	}

	raw(id) {
		if (!this._values.hasOwnProperty(id)) {
			return new Error('Identifier ' + id + 'is not defined');
		}

		if (this._raw.hasOwnProperty(id)) {
			return this._raw[id];
		}

		return this._values[id];
	}

	keys() {
		return Object.keys(this._values);
	}

	has(id) {
		return this._values.hasOwnProperty(id);
	}

	register(provider) {
		provider.call(this);
		return this;
	}
}

export default Konoha;
