# Konoha [![npm version](https://badge.fury.io/js/konoha.svg)](https://badge.fury.io/js/konoha)

Konoha is Dependency Injection Container that allows you take the advantage of the dependency injection principle in Javascript.

## Installation
```bash
npm install --save konoha
```

## Getting Started
First we need to create a new instace of Konoha. After that we can use methods like `set` and `get` to access it.

##### Using ES6
```javascript
import Konoha from 'konoha';

class Application extends Konoha {
    constructor() {
        super();
        this.set('isValid', true);
        this.set('user', {
            name: 'User Name',
            permissions: []
        });
        this.set('api', function() {
            return new Api();
        });

        this.get('api').post('/dummy/data');

        ...
    }
}
```

##### Pre-ES6 (NodeJS or CommonJS)
```javascript
var Konoha = require('konoha');
var app = new Konoha();

// you can chain any of the API methods
app.set('isValid', true)
    .set('user', {
        name: 'User Name',
        permissions: []
    })
    .set('api', function() {
        return new Api();
    });

app.get('api').post('/dummy/data');
```

## API

#### get(name)
Gets the value by the name.
```javascript
app.set('isValid', false);
app.set('foo', function() { return 'bar'; });

app.get('isValid'); // false
app.get('foo'); // 'bar'
```

#### set(name, value)
Sets a new attribute with given value.

You can set anything on the value. If the value is a function it will only be constructed/called once but it won't called until it is requested by `get`.
The function context will be the `app`, so you access to the `app` if you need it.
This shouldn't be used for setting/binding class definitions. Only the class intances should be stored.
```javascript
app.set('isValid', true);
app.set('user', {
    name: 'Full Name',
    isActive: true,
    permissions: []
});

// sets an API service
app.set('api.prefix', '/api');
app.set('api', function() {
    return new Api(this.get('api.prefix'));
});

var isValid = app.get('isValid');
var user = app.get('user');

// get API service
// This will initiate a new instance of the API
var api = app.get('api');
api.get('/some/rest/api');

// This time it will use existing API instance, since the function only gets called once
var api = app.get('api');
```

#### has(name)
Checks if there is anything registered under given name.
```javascript
app.has('version'); // false

app.set('version', '4.3.2');

app.has('version'); // true
```

#### keys()
Get all the registered names as an array.
```javascript
app.set('version', '4.3.2');
app.set('isValid', true);

app.keys(); // ['version', 'isValid']
```

#### raw(name)
Returns the original definition of the service.
```javascript
app.set('api', function() {
    return new Api();
});

var api = app.raw('api'); // the callback/function definition
```

#### extend(name, callback)
Allows you to extend/modify the registered service

Let's add a response handler to the API service that converts the response to JSON.
To do that we need to pass a function that takes one argument which will be the current value of the existing service.
It should return a new value that will replace the existing value.
```javascript
app.set('api', function() {
    return new Api();
});

app.extend('api', function(api) {
    api.addResponseHandler(new JsonResponse());
    return api;
});
```
Now every response you get should be converted into JSON.

#### factory(name, service)
Not everytime you want to get the cached (constructed) service. Use `factory` to create a service
that will be reconstructed/called everytime it gets requested by `get`.
```javascript
app.factory('person', function() {
    return new Person();
});

// Creates new Person instance
var person1 = app.get('person');

// Creates new Person instance again
var person2 = app.get('person');
```

#### protect(name, value)
Unlike `set` or `factory` which always returns the value of the service/function
the `protect` allows you define a service/function that will never called by `get`.
It's something that is needed if you want to store something without getting modified by `get`.
The `protect` will always return the definition of the service/function.
```javascript
app.protect('person', function(name) {
    var person = new Person(name);
    return person;
});

app.get('person'); // will return the function that takes a name argument

var siri = app.get('person')('Siri');
console.log(siri.getName()); // 'Siri'

var alexa = app.get('person')('Alexa');
console.log(alexa.getName()); // 'Alexa'
```

#### register(provider)
This function allows you to register services in bulk. The `provider` function that is passed
doesn't become a service. But it gets called immediately.
```javascript
app.register(function() {
    this.set('logger.level', 'debug');
    this.set('logger', function() {
        return new Logger(this.get('logger.level'));
    });
});
```
