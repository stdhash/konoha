'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Konoha = function () {
	function Konoha() {
		_classCallCheck(this, Konoha);

		this._values = {};
		this._factories = {};
		this._protected = {};
		this._frozen = {};
		this._raw = {};
	}

	_createClass(Konoha, [{
		key: 'get',
		value: function get(id) {
			if (this.raw.hasOwnProperty(id) || this._protected.hasOwnProperty(id) || typeof this._values[id] !== 'function') {
				return this._values[id];
			}

			if (this._factories.hasOwnProperty(id)) {
				return this._values[id].call(this);
			}

			this._raw[id] = this._values[id];
			this._values[id] = this._raw[id].call(this);
			this._frozen[id] = true;

			return this._values[id];
		}
	}, {
		key: 'set',
		value: function set(id, service) {
			if (this._frozen.hasOwnProperty(id)) {
				return new Error('Cannot override frozen service');
			}

			this._values[id] = service;
			return this;
		}
	}, {
		key: 'factory',
		value: function factory(id, _factory) {
			if (typeof _factory === 'function') {
				this._factories[id] = true;
			}

			this._values[id] = _factory;
			return this;
		}
	}, {
		key: 'protect',
		value: function protect(id, service) {
			this._protected[id] = true;
			this._values[id] = service;
			return this;
		}
	}, {
		key: 'extend',
		value: function extend(id, extender) {
			if (!this._values.hasOwnProperty(id)) {
				return new Error('Identifier ' + id + 'is not defined');
			}

			if (typeof extender !== 'function') {
				return new Error('Extension service definition is not a Class or Function');
			}

			var factory = this._values[id];
			var method = 'set';
			var call = true;

			if (this._factories.hasOwnProperty(id)) {
				method = 'factory';
			} else if (this._protected.hasOwnProperty(id)) {
				method = 'protect';
				call = false;
			}

			if (typeof factory !== 'function') {
				call = false;
			}

			return this[method](id, function () {
				return extender.call(this, call ? factory.call(this) : factory);
			});
		}
	}, {
		key: 'raw',
		value: function raw(id) {
			if (!this._values.hasOwnProperty(id)) {
				return new Error('Identifier ' + id + 'is not defined');
			}

			if (this._raw.hasOwnProperty(id)) {
				return this._raw[id];
			}

			return this._values[id];
		}
	}, {
		key: 'keys',
		value: function keys() {
			return Object.keys(this._values);
		}
	}, {
		key: 'has',
		value: function has(id) {
			return this._values.hasOwnProperty(id);
		}
	}, {
		key: 'register',
		value: function register(provider) {
			provider.call(this);
			return this;
		}
	}]);

	return Konoha;
}();

exports.default = Konoha;
